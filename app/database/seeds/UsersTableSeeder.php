<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		User::truncate();
		
		User::create(array(
			'id' => 1,
			'username' => 'admin',
			'password' => 'password',
			'name'     => 'Administrator'
		));
	}

}