<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder {

	public function run()
	{
		Post::truncate();

		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Post::create([

				'title' => $faker->word,
				'user_id' => 1,
				'text' => $faker->text,

			]);
		}
	}

}