<?php

class SessionsController extends BaseController {

	//Fields 

	/**
	 * Show the login form to sign in a user
	 * GET /login
	 *
	 * @return Response
	 */
	/*public function create()
	{
		if (Auth::check()) return Redirect::route('admin.articles.index');

		return View::make('sessions.create');
	}*/

	/**
	 * Authenticate a user credentials
	 * POST /login
	 *
	 * @return Response
	 */
	public function login()
	{
		$json = array('success' => false, 'msg' => null, 'redirect' => null);

		$credentials = array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        );

        $json = array('success' => false);

        if (Auth::attempt($credentials))
        {
        	$json = $json + Auth::user()->toArray();

            $json['redirect'] =  url() . '/dashboard#/';

            $json['success'] = true;

        } else {

        	$json['msg'] = 'Sin acceso';
        }

        return Response::json($json);
	}

	/**
	 * Destroy a user's session
	 * GET /logout
	 *
	 * @return Response
	 */
	public function destroy()
	{

		$json = array('success' => false, 'msg' => null);
		
		try {
			
			Auth::logout();

			$json['success'] = true;

		} catch (Exception $e) {
			
			$json['msg'] = "Error";
		}
		return Response::json($json);
	}

}