<?php

class WelcomeController extends BaseController {

	public function index() {

		return File::get(public_path() . '/templates/welcome.html');
	}

	public function dashboard() {
		
		return File::get(public_path() . '/templates/backend.html');
	}

}
