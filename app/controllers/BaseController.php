<?php

class BaseController extends Controller {

	/**
	 * Perform CSRF check on all post/put/patch/delete requests
	 * 
	 * @return void
	 */
	public function __construct()
    {
        //$this->beforeFilter('csrf', array('on' => array('delete', 'patch', 'post', 'put')));
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
