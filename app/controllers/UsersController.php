<?php

class UsersController extends BaseController {

	//Fields users
	/*
		username, password, name
	*/

	/**
	 * Display a listing of users
	 *
	 * @return Response
	 */
	public function index()
	{

		$users = User::orderBy('id', 'DESC')->where('id', '<>', Auth::user()->id)->paginate(6);
		
		return Response::json($users);
	}

	/**
	 * Show the form for creating a new user
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created user in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$json = array('success' => false, 'msg' => null);

		try 
		{
			$data = Input::only('username', 'password', 'name');

			//$validator = Validator::make(); //User::$createRules

			//if ($validator->fails()) throw new Exception($validator->messages());

			User::create($data);

			$json['success'] = true;

			$json['msg'] = 'Usuario creado';
		} 
		catch (Exception $e) 
		{
			Log::error($e->getMessage());

			$json['msg'] = 'Error';
		}

		return Response::json($json);
	}

	public function show($id) {
		
		//
	}

	/**
	 * Show the form for editing the specified user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$json = array('success' =>  false, 'msg' => null);

		try {
			
			$user = User::find($id);

			$json = $json + $user->toArray();

			$json['success'] = true;

		} catch (Exception $e) {
			
			$json['msg'] = 'Error';
		}

		return Response::json($json);
	}

	/**
	 * Update the specified user in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$json = array('success' => false, 'msg' => null);

		$data = Input::only('username', 'password', 'name');

		try 
		{
			//$validator = Validator::make($data = Input::only('username', 'password', 'name'), User::$updateRules); //'change-password',

			//if ($validator->fails()) throw new Exception($validator->messages());

			$user = User::find($id);

			if (isset($data['password']))
			{
				$user->password = $data['password'];				
			}

			$user->username = $data['username'];

			$user->name = $data['name'];

			$user->save();

			$json['success'] = true;

			$json['msg'] = 'Usuario actualizado';
		} 
		catch (Exception $e) 
		{
			Log::error($e->getMessage());

			$json['msg'] = 'Error';

		}

		return Response::json($json);
	}

	/**
	 * Remove the specified user from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$json = array('success' => false, 'msg' =>  null);
        
        try
        {
            
            User::destroy($id);

            $json['success'] = true;

            $json['msg'] = 'Usuario eliminado';
        }
        catch(Exception $e)
        {
            Log::error($e->getMessage());
            
            $json['msh'] = 'Error';
        }

        return Response::json($json);
	}

}
