<?php

class ProfileController extends BaseController {

	/**
	 * Display a listing of users
	 *
	 * @return Response
	 */
	public function index()
	{

		$json = array('msg' => null, 'redirect' => null);

		try {
			
			$json = $json + Auth::user()->toArray();

		} catch (Exception $e) {
			
			$json['msg'] = 'Error con su cuenta';

			$json['redirect'] = '/';
		}

		return Response::json($json);
	}

	/**
	 * Show the form for creating a new user
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created user in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	public function show($id) {
		
		//
	}

	/**
	 * Show the form for editing the specified user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$json = array('success' =>  false, 'msg' => null);

		try {
			
			$user = User::find($id);

			$json = $json + $user->toArray();

			$json['success'] = true;

		} catch (Exception $e) {
			
			$json['msg'] = 'Error';
		}

		return Response::json($json);
	}

	/**
	 * Update the specified user in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$json = array('success' => false, 'msg' => null);

		$data = Input::only('username', 'password', 'name');

		try 
		{
			//$validator = Validator::make($data = Input::only('username', 'password', 'name'), User::$updateRules); //'change-password',

			//if ($validator->fails()) throw new Exception($validator->messages());

			$user = User::find($id);

			if (isset($data['password']))
			{
				$user->password = $data['password'];				
			}

			$user->username = $data['username'];

			$user->name = $data['name'];

			$user->save();

			$json['success'] = true;

			$json['msg'] = 'Usuario actualizado';
		} 
		catch (Exception $e) 
		{
			Log::error($e->getMessage());

			$json['msg'] = 'Error';

		}

		return Response::json($json);
	}

	/**
	 * Remove the specified user from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
	}

}
