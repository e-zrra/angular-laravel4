<?php

class PostsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /posts
	 *
	 * @return Response
	 */
	public function index()
	{

		$posts = Post::orderBy('id', 'DESC')->paginate(5);;

		return Response::json($posts);
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /posts/create
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /posts
	 *
	 * @return Response
	 */
	public function store()
	{
		$json = array('success' => false, 'redirect' => null);

		$data = Input::only('title', 'text');

		try 
		{
			//$validator = Validator::make($data = Input::only('username', 'password', 'name'), User::$updateRules); //'change-password',

			//if ($validator->fails()) throw new Exception($validator->messages());

			$post = new Post();

			$post->title = $data['title'];

			$post->text = $data['text'];

			$post->save();

			$json['success'] = true;

			$json['redirect'] =  url() . '/dashboard#/posts/'.$post->id;
		} 
		catch (Exception $e) 
		{
			Log::error($e->getMessage());

			$json['msg'] = 'Erroreeee'.$e;

		}

		return Response::json($json);
		
	}

	/**
	 * Display the specified resource.
	 * GET /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = Post::find($id);

		return Response::json($post);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /posts/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = Post::find($id);

		return Response::json($post);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$json = array('success' => false, 'msg' => null);

		$data = Input::only('title', 'text');

		try 
		{
			//$validator = Validator::make($data = Input::only('username', 'password', 'name'), User::$updateRules); //'change-password',

			//if ($validator->fails()) throw new Exception($validator->messages());

			$post = Post::find($id);

			$post->title = $data['title'];

			$post->text = $data['text'];

			$post->save();

			$json['success'] = true;

			$json['msg'] = 'Post actualizado';
		} 
		catch (Exception $e) 
		{
			Log::error($e->getMessage());

			$json['msg'] = 'Error';

		}

		return Response::json($json);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}