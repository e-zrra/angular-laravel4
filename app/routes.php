<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/*
Blade::setContentTags('<%%', '%%>'); 		// for variables and all things Blade

Blade::setEscapedContentTags('<%', '%>'); 	// for escaped data
*/

Route::get('/', array('as' => 'home', 'uses' => 'WelcomeController@index'));

Route::get('/dashboard', array('before' => 'auth', 'uses' => 'WelcomeController@dashboard'));

Route::post('auth/login', array('uses' => 'SessionsController@login'));

Route::get('auth/logout', array('before' => 'auth', 'uses' => 'SessionsController@destroy'));

Route::group(array('prefix' => 'api', 'before' => 'auth'), function () {

	Route::resource('users', 'UsersController', array('only' => array('index', 'store', 'destroy', 'edit', 'update')));

	Route::resource('profile', 'ProfileController', array('only' => array('index', 'edit', 'update')));

	Route::resource('posts', 'PostsController', array('only' => array('index', 'show','store', 'destroy', 'edit', 'update')));

});

//Route::get('{angular?}', [ 'uses' => 'WelcomeController@index' ])->where('angular', '.*');

/*App::missing(function ($exception) {
	//return View::make('index');
});*/