<?php

/**
 * Set application locale in routes.php
 * 
 * @return mixed
 */
function set_applicaton_locale()
{
	$languages = array('en', 'es');

	$locale = Request::segment(1);

	if (in_array($locale, $languages))
	{
		App::setLocale($locale);

		return $locale;
	}

	return null;
}

/**
 * Get locale dropdown options
 * 
 * @return array
 */
function get_locale_options()
{
	return array(
		'es'  => 'Spanish',
		'en'  => 'English'
	);
}




function img_tag($src, $alt, $attributes = '')
{
	$format = '<img src="%s" alt="%s" %s>';

    $uri = "assets/{$src}";
    
    return sprintf($format, url($uri), $alt, $attributes);
}


/**
 * Display search result count
 * 
 * @param object
 * @return string
 */
function get_search_results($articles)
{
	$count = count($articles);

	if ($count === 1)
	{
		return trans('strings.result', array('count' => $count));
	}

	return trans('strings.results', array('count' => $count));
}