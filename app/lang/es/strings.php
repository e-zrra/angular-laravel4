 <?php

return array(

	'search' => 'Buscar',

	'program' => 'Programa',

	'events' => 'Eventos',

	'workshops' => 'Talleres',

	'accommodation' => 'Hospedaje',

	'more_info' => 'Más información',

	'post' => 'Publicación',

	'posts' => 'Publicaciones',

	'return' => 'Regresar',

	'results_by' => 'Resultados de',

	'share_twitter' => 'Compartir por Twitter',

	'share_facebook' => 'Compartir por Facebook',

	'share_email' => 'Compartir por correo',

	'rights_reserved' => 'Todos los derechos reservados. Por .',

	'sponsor' => 'Patrocinador',

	'today' => 'Hoy',

	'tomorrow' => 'Mañana',

	'empty_posts' => 'Hoy aún no hay nuevas publicaciones.',

	'read_more' => 'Leer historia completa',

	'post_date' => ':day de :month @ :time',

	'results' => ':count resultados',

	'result' => ':count resultado',

	'gallery' => 'Galería',

	'event_placeholder' => ':day de Octubre @ 10:00 a.m.',

	'buy_tickets' => 'Comprar boletos',

	'profile' => 'Perfil',

	'share' => 'Compartir',

	'free_event' => 'Evento gratuito'

);