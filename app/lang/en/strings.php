 <?php

return array(

	'search' => 'Search',

	'program' => 'Program',

	'events' => 'Events',

	'workshops' => 'Workshops',

	'accommodation' => 'Accommodation',

	'more_info' => 'More info',

	'post' => 'Post',

	'posts' => 'Posts',

	'return' => 'Back',

	'results_by' => 'Results by',

	'share_twitter' => 'Share to Twitter',

	'share_facebook' => 'Share to Facebook',

	'share_email' => 'Send by email',

	'rights_reserved' => 'All rights reserved. By.',

	'sponsor' => 'Sponsor',

	'today' => 'Today',

	'tomorrow' => 'Tomorrow',

	'empty_posts' => 'No posts yet.',

	'read_more' => 'Read full story',

	'post_date' => ':month :day @ :time',

	'results' => ':count results',

	'result' => ':count result',

	'gallery' => 'Photo gallery',

	'event_placeholder' => 'October :day @ 10:00 a.m.',

	'buy_tickets' => 'Buy tickets',

	'profile' => 'Profile',

	'share' => 'Share',

	'free_event' => 'Free event'

);