app.directive('paginator', ['Users', 'Posts' ,'$http', function (Users, Posts, $http) {
    return {
        restrict: 'E',
        scope: {
            results: '=',
            entity: '@'
        },
        templateUrl: 'templates/helpers/pagination.html',
        link: function (scope) {

            scope.nextPage = function () {
                if (scope.current_page < scope.totalPages) {
                    scope.current_page++;
                }
            };

            scope.prevPage = function () {
                if (scope.current_page > 1) {
                    scope.current_page--;
                }
            };

            scope.firstPage = function () {
                scope.current_page = 1;
            };

            scope.last_page = function () {
                scope.current_page = scope.totalPages;
            };

            scope.setPage = function (page) {
                scope.current_page = page;
            };

            var paginate = function (results, oldResults) {
                //console.log(oldResults)
                //console.log(results)

                if (oldResults === results) return;

                scope.current_page = results.current_page;
                scope.total = results.total;
                scope.totalPages = results.last_page;
                scope.pages = [];

                for (var i = 1; i <= scope.totalPages; i++) {
                    scope.pages.push(i);
                }
            };

            var pageChange = function (newPage, last_page) {
                
                if (newPage == last_page) return;

                switch (scope.entity) {
                    case 'users':
                        Users.index(newPage)
                        .success(function (data) {

                            angular.copy(data.data, scope.results.data);

                            scope.results.current_page = data.current_page;
                            
                        }).error(function () {
                            alertBox('danger', 'Information', 'Error', 'Ok');
                        });
                    break;
                    case 'posts':
                        Posts.index(newPage)
                        .success(function (data) {

                            angular.copy(data.data, scope.results.data);

                            scope.results.current_page = data.current_page;
                            
                        }).error(function () {
                            alertBox('danger', 'Information', 'Error', 'Ok');
                        });
                    break;
                }

            };

            scope.$watch('results', paginate);
            scope.$watch('current_page', pageChange);
            scope.$watch('entity', pageChange);

            //scope.$watch('current_page', scope.prevPage);

            //scope.$watch('current_page', prev_Page);
        }
    }
}]);