var app = angular.module('app', ['ngRoute','ngSanitize', 'ui.bootstrap']); //ngResource

app.config(function($routeProvider) {

  $routeProvider.when('/auth/login', {
    templateUrl: 'templates/auth/login.html',
    controller: 'LoginCtrl'
  });

  $routeProvider.when('/users', {
    templateUrl: 'templates/users/index.html',
    controller: 'UsersCtrl'
  });

  $routeProvider.when('/posts', {
    templateUrl: 'templates/posts/index.html',
    controller: 'PostsCtrl'
  }).when('/posts/:post_id', {
    templateUrl: 'templates/posts/show.html',
    controller: 'PostsCtrl'
  }).when('/posts/:post_id/edit', {
    templateUrl: 'templates/posts/edit.html',
    controller: 'PostsCtrl'
  }).when('/posts/post/create', {
    templateUrl: 'templates/posts/create.html',
    controller: 'PostsCtrl'
  });

  $routeProvider.otherwise({ redirectTo: '/' });

});

app.run(function ($rootScope, $location, Authenticate) {

});