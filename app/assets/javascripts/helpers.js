
// document: referer

$('#referer').on('click', function() {
    location.href = document.referrer;
});

// bootbox: alert dialog

function alertBox(type, header, content, buttonText) {

    if( ! buttonText) {
        buttonText = 'Ok';
    };
    
    bootbox.dialog({
        className: 'my-custom-dialog ' + type + '-dialog',
        message: content,
        title: header,
        buttons: {
            main: {
                className: 'btn-' + type + ' btn-sm',
                label: buttonText
            }
        }
    });
}

// bootbox: confirm dialog

function confirmBox(header, content, callback) {
    
    bootbox.dialog({
        className: 'my-custom-dialog warning-dialog',
        message: content,
        title: header,
        buttons: {
            cancel: {
                className: 'btn-default btn-sm',
                label: 'No'
            },
            main: {
                className: 'btn-warning btn-sm',
                label: 'Yes',
                callback: callback
            }
        }
    });
}

// delete a record from the database

/*function destroyRecord(anchor) {

    var form = anchor.closest('form');
    var text = anchor.text();

    form.ajaxSubmit({
        beforeSend: function() {
            anchor.text('Deleting...').prop('disabled', true);
        },
        error: function() {
            showDialog('ajax.error');
        },
        success: function(response) {
            if ( ! response.success) return showDialog('ajax.failed');
            location.reload();
        },
        complete: function() {
            anchor.text(text).prop('disabled', false);
        }
    });
}*/
