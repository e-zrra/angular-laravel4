app.controller('LoginCtrl', function ($scope, Authenticate, $location) {

		$scope.login = function () {
			if ($scope.credentials) {
				Authenticate.login($scope.credentials)
					.success(function (data) {
						if (data.success) {
							window.location.href = data.redirect;
						} else {
							alertBox('primary', 'Information', data.msg, 'Ok');
						}
					}).error(function (data) {
						alertBox('primary', 'Information', 'Error', 'Ok');
					});
			};
		}

		$scope.logout = function () {
			Authenticate.logout()
				.success(function (data) {
					alertBox('primary', 'Information', 'Vuelve pronto!', 'Ok');
					if (data.success) {
						window.location.href = '/';
					};
				}).error(function (data) {
					alertBox('primary', 'Information', 'Error', 'Ok');
				});
		}
	
	});

//$location.path(data.redirect);