'use strict';
app.controller('PostsCtrl', function ($scope, Posts, $routeParams) {

	$scope.loading = true;

	//List posts
	var posts = function () {
		var page = null;
		
		if(document.URL.indexOf('?page=') > -1) {
            page = window.location.href;
            page = page.substring(page.indexOf('?page='));
            page = page.replace('?page=', '');
        };

		Posts.index(page)
			.success(function (data) {

			$scope.posts = data.data;

			$scope.loading = false;

			$scope.results = data;

		}).error(function () {
			alertBox('danger', 'Information', 'Error', 'Ok');
		});
	}

	posts();

	//Show post
	var post = function (post_id) {
		Posts.show(post_id)
			.success(function (data) {

			$scope.post = data;

			$scope.loading = false;

		}).error(function () {
			alertBox('danger', 'Information', 'Error', 'Ok');
		});
	}

	//Function Edit posts

	var edit_post = function (post_id) {
		
		Posts.edit(post_id)
			.success(function (data) {

			$scope.post = data;

			$scope.loading = false;

		}).error(function () {
			alertBox('danger', 'Information', 'Error', 'Ok');
		});
	}

	//check if url has id called post_id
	if ($routeParams.post_id) {
		//Show
		$scope.post_id = $routeParams.post_id;
		post($scope.post_id);

		//Edit
		$scope.post_id = $routeParams.post_id;
		edit_post($scope.post_id);

	};

	//Function update post
	$scope.change_save = function (post) {
		Posts.update(post)
			.success(function (data) {
				alertBox('success', 'Information', data.msg, 'Ok');
			}).error(function () {
				alertBox('danger', 'Information', 'Error', 'Ok');
			})
	}

	$scope.save = function (post) {

		Posts.store(post)
			.success(function (data) {
				if (data.success) {
					window.location.href = data.redirect;
				} else {
					alertBox('primary', 'Information', data.msg, 'Ok');
				}
			}).error(function () {
				alertBox('danger', 'Information', 'Erroree', 'Ok');
			})
	}


});

//NOTA: Se puede crear diferentes controladores para cada funcción de la entidad Posts (show, create), 
//Eje: PostsEditCtrl, y en el routeProvider agregar
//el controlador por cada ruta.