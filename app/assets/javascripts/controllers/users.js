'use strict';
app.controller('UsersCtrl',  function ($scope, $modal, Users) {

	$scope.loading = true;

	$scope.user = [];

	//List users
	var users = function () {

		var page = null;
		
		if(document.URL.indexOf('?page=') > -1) {
            page = window.location.href;
            page = page.substring(page.indexOf('?page='));
            page = page.replace('?page=', '');
        };

       	
		Users.index(page)
		.success(function (data) {
			
			$scope.users = data.data;

			$scope.loading = false;

			$scope.results = data;

			angular.element('.current-true').css('font-size', '20px');//addClass('active-pagination');

	       	

		}).error(function () {
			alertBox('danger', 'Information', 'Error', 'Ok');
		});
	}
	//Init function list
	users();

	var modal = function () {
		$scope.modalInstance = $modal.open({
			scope: $scope,
	    	templateUrl: 'form_user.html',
	    	controller: function ($scope, $modalInstance, $log, user) {
	    		
	    		var action = 'store';

	    		if (user._id) action = 'update';

	    		$scope.ok = function () {
	    			switch(action) {
	    				case 'store':
	    					Users.store($scope.user)
								.success(function (data) {
									if (data.success) {
										
										if (data.msg) {

											alertBox('primary', 'Information', data.msg, 'Ok');
										};
										
										$scope.modalInstance.close();

										users();

									} else {
										alertBox('danger', 'Information', data.msg, 'Ok');

									}
								}).error(function (data) {
									alertBox('danger', 'Information', 'Error', 'Ok');
								});
	    				break;
	    				case 'update':

	    					Users.update($scope.user)
		    					.success(function (data) {
		    						if (data.success) {

		    							if (data.msg) {

											alertBox('primary', 'Information', data.msg, 'Ok');
										};

		    							$scope.modalInstance.close();

		    							users();

		    						} else {

		    							alertBox('danger', 'Information', data.msg, 'Ok');
		    						}
		    						
		    					}).error(function (data) {
		    						
		    						alertBox('danger', 'Information', 'Error', 'Ok');
		    					});
	    				break;
	    				default:
	    					alert('Error!');
	    			}
				};
				$scope.cancel = function () {
					$scope.modalInstance.dismiss('cancel');
				};
	    	},
	    	resolve: {
	    		user: function () {
	    			return $scope.user;
	    		}
	    	}
	    });
	}

	//Function new user
	$scope.new_user = function () {
		$scope.user = {
			_id: null,
			name: null,
			username: null,
			password: null
		};

		modal();
	};

	//Function delete user
	$scope.delete_user = function (user_id) {

		var delete_user = function () {

			Users.destroy(user_id)
				.success(function (data) {

					if (data.success) {

						if (data.msg) {

							alertBox('primary', 'Information', data.msg, 'Ok');
						};

						users();

					} else {

						alertBox('danger', 'Information', data.msg, 'Ok');
					}

				}).error(function (data) {
					
					alertBox('danger', 'Information', 'Error', 'Ok');
				});
		}

		confirmBox('Information', 'Are you sure to delete this row?', delete_user);

	}

	//Function edit user
	$scope.edit_user = function (user_id) {

		Users.edit(user_id)
			.success(function (data) {

				console.log(data)

				if (data.success) {

					//angular.copy(data.data, scope.results.data);
					console.log(data)
					$scope.user._id = data.id;
					$scope.user.name = data.name;
					$scope.user.username = data.username;
					$scope.user.password = null;

					modal();

				} else {

					alertBox('danger', 'Information', data.msg, 'Ok');
				}
			}).error(function (data) {

				alertBox('danger', 'Information', 'Error', 'Ok');
			});
	}

})