app.factory('Authenticate', function ($http, $sanitize, Session) {

    	var cacheSession   = function () {
			Session.set('authenticated', true);
		};

		var uncacheSession = function() {
			Session.unset('authenticated');
		};

		/* var loginError = function(response) {
			//FlashService.show(response.flash);
		};*/

    	var sanitizeCredentials = function(credentials) {
		    return {
		      	username: $sanitize(credentials.username),
		      	password: $sanitize(credentials.password)
		    	//,csrf_token: CSRF_TOKEN
		    };
		};

        return {
		    login: function(credentials) {

		    	var login = $http({
		    		url: '/auth/login',
		    		method: 'POST',
		    		data: sanitizeCredentials(credentials),
		    		responseType: 'json'
		    	});

		    	login.success(function () {
		    		
		    		cacheSession;

		    		var profile = login.$$state.value.data;

		    		Session.set_object('profile', profile);

		    		var p = JSON.parse(Session.get('profile'));

		    	});

		    	return login;
		    },

		    logout: function() {
		    	
		    	var logout = $http({
		    		url: '/auth/logout',
		    		method: 'GET',
		    		responseType: 'json'
		      	});

		      	Session.unset('profile');

		      	logout.success(uncacheSession);

		      	return logout;
		    },

		    isLoggedIn: function() {
		      return Session.get('authenticated');
		    }
	  	};
    })