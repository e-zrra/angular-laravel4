app.factory('Users', function ($http, $sanitize) {

		var sanitize = function(data) {
		    return {
		      	username: $sanitize(data.username),
		      	password: $sanitize(data.password),
		      	name: 	  $sanitize(data.name)
		    };
		};

		return {
			index: function (page) {
				return $http({
					url: '/api/users?page=' + page,
					method: 'GET',
					responseType: 'json'
				});
			},
			store: function (data) {
				return $http({
					url: '/api/users',
					method: 'POST',
					data: sanitize(data),
					responseType: 'json'
				});
			},
			edit: function (user_id) {
				return $http({
					url: '/api/users/' + user_id + '/edit',
					method: 'GET',
					responseType: 'json'
				});
			},
			update: function (data) {
				return $http({
					url: '/api/users/' + data._id,
					method: 'PUT',
					data: sanitize(data),
					responseType: 'json'
				});
			},
			destroy: function (user_id) {
				return $http({
					url: '/api/users/' + user_id,
					method: 'DELETE',
					responseType: 'json'
				});
			}
		}
	});