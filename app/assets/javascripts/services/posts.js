app.factory('Posts', function ($http, $sanitize) {

		var sanitize = function(data) {
		    return {
		      	title: $sanitize(data.title),
		      	text: $sanitize(data.text)
		    };
		};

		return {
			index: function (page) {
				return $http({
					url: '/api/posts?page=' + page,
					method: 'GET',
					responseType: 'json'
				});
			},
			show: function (id) {
				return $http({
					url: '/api/posts/' + id,
					method: 'GET',
					responseType: 'json'
				});
			},
			edit: function (id) {
				return $http({
					url: '/api/posts/' + id + '/edit',
					method: 'GET',
					responseType: 'json'
				});
			},
			update: function (data) {
				return $http({
					url: '/api/posts/' + data.id,
					method: 'PUT',
					data: sanitize(data),
					responseType: 'json'
				});
			},
			store: function (data) {
				return $http({
					url: '/api/posts',
					method: 'POST',
					data: sanitize(data),
					responseType: 'json'
				});
			}
		}
	});