app.factory('Profile', function ($http, Session) {

	return {
		get: function () {
			return $http({
				url: '/api/profile',
				method: 'GET',
				responseType: 'json'
			});
		}
	}
});