
// messages: dialogs

function showDialog(type) {

	switch(type) {

		case 'ajax.error':
		    alertBox('info', 'Server connection', 'Oops, it seems we lost the connection with the server.<br><br>' + 
		        'This issue could be a result of: <br>' + 
		        '&bull; There is not internet connection. <br>' + 
		        '&bull; The server is under maintenance. <br>' + 
		        "&bull; The server can't respond at this moment. <br><br>");
			break;

		case 'ajax.failed':
			alertBox('danger', 'Server Response', 'Sorry, your request cannot be submitted at this time, please try again');
			break;

		case 'ajax.success':
			alertBox('success', 'Success', 'Your changes were saved');
			break;

		case 'session.invalid':
			alertBox('danger', 'Login', 'Oops, your credentials are invalid, please try again.');
			break;
		
	}

}
